package br.com.fdantasb.socialbooks.domain;

import br.com.fdantasb.socialbooks.repository.AutoresRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AutorTest {


    @Autowired
    private AutoresRepository autoresRepository;

    private Autor autor;
    private List<Autor> autorList;

    @Before
    public void setup (){
        autorList = autoresRepository.findAll();
        autor = new Autor();
    }

    @Test
    public void setNome() {
        autor.setNome("Joaquim Nabuco");
        assertNotNull(autor.getNome());
    }

    @Test
    public void setNascimento() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1894,8,19);
        autor.setNascimento(calendar.getTime());
        assertNotNull(autor.getNascimento());
    }

    @Test
    public void setNacionalidade() {
        autor.setNacionalidade("Brasileira");
        assertNotNull(autor.getNacionalidade());
    }

    @Test
    public void setLivros() {
        autor.setLivros(Collections.emptyList());
        assertNotNull(autor.getLivros());
    }
}