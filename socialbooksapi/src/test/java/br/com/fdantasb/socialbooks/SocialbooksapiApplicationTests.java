package br.com.fdantasb.socialbooks;

import br.com.fdantasb.socialbooks.domain.Autor;
import br.com.fdantasb.socialbooks.domain.Comentario;
import br.com.fdantasb.socialbooks.domain.Livro;
import br.com.fdantasb.socialbooks.repository.AutoresRepository;
import br.com.fdantasb.socialbooks.repository.ComentariosRepository;
import br.com.fdantasb.socialbooks.repository.LivrosRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SocialbooksapiApplicationTests {


	@Autowired
	private AutoresRepository autoresRepository;
	@Autowired
	private LivrosRepository livrosRepository;
	@Autowired
	private ComentariosRepository comentariosRepository;


	@Test
	public void contextLoads() {
		Autor machado = new Autor();
		machado.setNome("Machado de Assis");
		machado.setNacionalidade("Brasileira");

		Calendar nascimentoMachado = Calendar.getInstance();
		nascimentoMachado.set(1839, 6, 21);
		machado.setNascimento(nascimentoMachado.getTime());

		machado = autoresRepository.save(machado);
		Assert.assertNotNull(machado);

		Livro memoriasBras = new Livro();
		memoriasBras.setNome("Memórias Póstumas de Brás Cubas");
		memoriasBras.setAutor(machado);
		memoriasBras.setEditora("Oxford University Press");
		memoriasBras.setResumo("Memórias Póstumas de Brás Cubas é um livro de Machado de Assis, publicado como folhetim entre março e dezembro de 1880 na Revista Brasileira.");

		Calendar publicacaoMemorias = Calendar.getInstance();
		publicacaoMemorias.set(1, 1, 1881);
		memoriasBras.setPublicacao(publicacaoMemorias.getTime());

		memoriasBras = livrosRepository.save(memoriasBras);
		Assert.assertNotNull(memoriasBras);

		Comentario comentario = new Comentario();
		comentario.setData(Calendar.getInstance().getTime());
		comentario.setUsuario("TestCase");
		comentario.setLivro(memoriasBras);
		comentario.setTexto("Uma excelente obra da literatura brasileira.");

		comentariosRepository.save(comentario);
		Assert.assertNotNull(comentario);

	}

}
