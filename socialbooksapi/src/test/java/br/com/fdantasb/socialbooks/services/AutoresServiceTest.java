package br.com.fdantasb.socialbooks.services;

import br.com.fdantasb.socialbooks.domain.Autor;
import br.com.fdantasb.socialbooks.repository.AutoresRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AutoresServiceTest {

    @Autowired
    private AutoresService autoresService;

    @Test
    public void salvarAutor() {
        Autor jorgeAmado = criaAutorJorgeAmado();
        Autor autor = autoresService.salvarAutor(jorgeAmado);
        assertNotNull(autor);
    }

    @Test
    public void buscar() {

        Autor autor = autoresService.buscar(1l);
        assertNotNull(autor);
    }

    @Test
    public void listarAutores() {
        List<Autor> all = autoresService.listarAutores();
        assertNotNull(all);
    }

    @Test
    public void update() {
        Autor autor = autoresService.buscar(1l);
        autor.setNacionalidade("Brasileira");
        autoresService.update(autor);
    }

    private Autor criaAutorJorgeAmado() {
        Autor autor = new Autor();
        autor.setNome("Jorge Amado");
        autor.setNacionalidade("Americana");
        Calendar calendar = Calendar.getInstance();
        calendar.set(1912, 8, 10);
        Date nascimento = calendar.getTime();
        autor.setNascimento(nascimento);

        return autor;
    }
}