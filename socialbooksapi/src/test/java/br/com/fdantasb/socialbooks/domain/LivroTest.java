package br.com.fdantasb.socialbooks.domain;

import br.com.fdantasb.socialbooks.repository.LivrosRepository;
import br.com.fdantasb.socialbooks.services.LivrosService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LivroTest {

    @Autowired
    private LivrosRepository livrosRepository;

    List<Livro> livroList;

    private Livro livro;

    @Before
    public void setup(){
        livroList = livrosRepository.findAll();
        livro = new Livro();
    }

    @Test
    public void setNome() {
        livro.setNome("Dom Casmurro");
    }

    @Test
    public void setPublicacao() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1889,1,1);
        livro.setPublicacao(calendar.getTime());
        assertNotNull(livro.getPublicacao());
    }

    @Test
    public void setEditora() {
        livro.setEditora("Livraria Garnier");
    }

    @Test
    public void setResumo() {
        livro.setResumo("O livro apresenta o relato de Bentinho, que se crê traído pela mulher, Capitu, e pelo seu melhor amigo. É com orgulho que esta editora oferece aos seus leitores, em formato de bolso, este romance machadiano.");
    }

}