package br.com.fdantasb.socialbooks.services;

import br.com.fdantasb.socialbooks.domain.Autor;
import br.com.fdantasb.socialbooks.domain.Comentario;
import br.com.fdantasb.socialbooks.domain.Livro;
import br.com.fdantasb.socialbooks.repository.AutoresRepository;
import br.com.fdantasb.socialbooks.repository.LivrosRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LivrosServiceTest {


    @Autowired
    private LivrosService livrosService;

    @Autowired
    private AutoresRepository autoresRepository;

    private Autor firstAutor;
    private List<Livro> livroList;

    @Before
    public void setup(){
        firstAutor = autoresRepository.findAll().stream().findFirst().get();
        livroList = livrosService.listar();
    }

    @Test
    public void salvar() {
        Livro livro = new Livro();
        livro.setNome("Quincas Borba");
        livro.setResumo("Neste romance da maturidade do autor, a história do provinciano Rubião — herdeiro da fortuna do idiossincrático filósofo Quincas Borba — e dos tipos urbanos da corte que o levam à ruína é narrada com o distanciamento, o ceticismo e o senso de humor implacável de que só Machado de Assis era capaz.");
        livro.setEditora("Editora Qualquer");

        Calendar calendar = Calendar.getInstance();
        calendar.set(1,1,1891);
        livro.setPublicacao(calendar.getTime());

        livro.setAutor(firstAutor);

        livro = livrosService.salvar(livro);

        assertNotNull(livro.getId());

    }

    @Test
    public void listar() {
        List<Livro> listar = livrosService.listar();
        assertNotNull(listar);
    }

    @Test
    public void buscar() {
        List<Livro> listar = livrosService.listar();
        Livro livro = livrosService.buscar(listar.stream().findFirst().get().getId());
        assertNotNull(livro);
    }

    @Test
    public void atualizar() {
        Livro livro = livrosService.buscar(livroList.get(livroList.size() -1 ).getId());
        livro.setEditora("Livraria Garnier");

        livrosService.atualizar(livro);
    }

    @Test
    public void salvarComentario() {

        Comentario comentario = new Comentario();
        comentario.setTexto("Mais uma excelente obra do autor!");
        comentario.setUsuario("Test User");
        comentario.setData(Calendar.getInstance().getTime());

        comentario = livrosService.salvarComentario(livroList.get(livroList.size() -1 ).getId(), comentario);

        assertNotNull(comentario.getId());

    }

    @Test
    public void comentarios() {
        List<Comentario> comentarios = livrosService.comentarios(livroList.get(livroList.size() -1 ).getId());

        assertNotNull(comentarios);
    }

    @Test
    public void deletar() {
        Livro last = livroList.stream().findFirst().get();

        livrosService.deletar(last.getId());
    }
}