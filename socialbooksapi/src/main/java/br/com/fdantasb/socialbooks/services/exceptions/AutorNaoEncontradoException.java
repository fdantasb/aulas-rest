package br.com.fdantasb.socialbooks.services.exceptions;

public class AutorNaoEncontradoException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3702633518512978713L;
	
	public AutorNaoEncontradoException(String message, Throwable cause) {
		super(message, cause);
	}

	public AutorNaoEncontradoException(String message) {
		super(message);
	}

}
