package br.com.fdantasb.socialbooks.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.fdantasb.socialbooks.domain.Comentario;

public interface ComentariosRepository extends JpaRepository<Comentario, Long>{

}
