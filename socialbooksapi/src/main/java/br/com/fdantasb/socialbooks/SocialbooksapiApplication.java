package br.com.fdantasb.socialbooks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"br.com.fdantasb.socialbooks.domain", "br.com.fdantasb.socialbooks.resources", 
		"br.com.fdantasb.socialbooks.repository", "br.com.fdantasb.socialbooks.services", "br.com.fdantasb.socialbooks.handler", "br.com.fdantasb.socialbooks.config"})
public class SocialbooksapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SocialbooksapiApplication.class, args);
	}
}
