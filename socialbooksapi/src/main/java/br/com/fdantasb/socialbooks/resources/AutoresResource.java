package br.com.fdantasb.socialbooks.resources;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.fdantasb.socialbooks.domain.Autor;
import br.com.fdantasb.socialbooks.services.AutoresService;

@Controller
@RequestMapping("/autores")
public class AutoresResource {

	private static Logger LOG = LoggerFactory.getLogger(AutoresResource.class);
	
	@Autowired
	private AutoresService autoresService;
	
	@RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<List<Autor>> listar() {
		LOG.info("Entra no metodo de listar autores.");
		List<Autor> listaAutores = autoresService.listarAutores();
		LOG.info("Quantidade de autores encontrados: " + listaAutores.size());
		return ResponseEntity.status(HttpStatus.OK).body(listaAutores);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> salvar(@Valid @RequestBody Autor autor){
		autor = autoresService.salvarAutor(autor);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(autor.getId()).toUri();
		LOG.info("URI criada: " + uri.getPath());
		return ResponseEntity.created(uri).build();
	}
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizar(@RequestBody Autor autor) {
		LOG.info("Update no autor de id: " + autor.getId());
		autoresService.update(autor);
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Autor> buscar(@PathVariable Long id) {
		LOG.info("Entra no método de busca de autor por ID.");
		Autor autor = autoresService.buscar(id);
		return ResponseEntity.status(HttpStatus.OK).body(autor);
	}


}
