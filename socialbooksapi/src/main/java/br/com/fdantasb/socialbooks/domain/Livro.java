package br.com.fdantasb.socialbooks.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.hibernate.annotations.CascadeType;


@Entity
public class Livro {
	
	@Id
	@JsonInclude(Include.NON_NULL)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotEmpty(message = "O campo de nome não pode ser vazio.")
	private String nome;
	@JsonInclude(Include.NON_NULL)
	@JsonFormat(pattern = "dd/MM/yyyy")
	@NotNull(message = "O preenchimento da data de publicação é obrigatória.")
	private Date publicacao;
	@JsonInclude(Include.NON_NULL)
	@NotNull(message = "O preenchimento da editora é obrigatória.")
	private String editora;
	@JsonInclude(Include.NON_NULL)
	@NotNull(message = "O preenchimento do resumo é obrigatório.")
	@Size(max = 1500, message = "O resumo não pode ter mais de 1500 caracteres.")
	private String resumo;
	@OneToMany(mappedBy = "livro", fetch = FetchType.EAGER)
	@JsonInclude(Include.NON_EMPTY)
	@Cascade(CascadeType.DELETE)
	private List<Comentario> comentarios;
	@ManyToOne
	@JoinColumn(name = "autor")
	@JsonInclude(Include.NON_NULL)
	@NotNull(message = "É necessário que o livro tenha um autor associado.")
	private Autor autor;
	
	public Livro() {
	}


	@Override
	public String toString() {
		return "Nome: " + this.nome +
				"\nAutor: " + this.autor.getNome();
	}

	public Livro(String nome) {
		this.nome = nome;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getPublicacao() {
		return publicacao;
	}
	public void setPublicacao(Date publicacao) {
		this.publicacao = publicacao;
	}
	public String getEditora() {
		return editora;
	}
	public void setEditora(String editora) {
		this.editora = editora;
	}
	public String getResumo() {
		return resumo;
	}
	public void setResumo(String resumo) {
		this.resumo = resumo;
	}
	public List<Comentario> getComentarios() {
		return comentarios;
	}
	public void setComentarios(List<Comentario> comentarios) {
		this.comentarios = comentarios;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}
	
}
