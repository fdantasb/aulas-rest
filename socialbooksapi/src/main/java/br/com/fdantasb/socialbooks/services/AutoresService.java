package br.com.fdantasb.socialbooks.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fdantasb.socialbooks.domain.Autor;
import br.com.fdantasb.socialbooks.repository.AutoresRepository;
import br.com.fdantasb.socialbooks.services.exceptions.AutorExistenteException;
import br.com.fdantasb.socialbooks.services.exceptions.AutorNaoEncontradoException;

@Service
public class AutoresService {

	private static Logger LOG = LoggerFactory.getLogger(AutoresService.class);

	@Autowired
	private AutoresRepository autoresRepository;
	
	public List<Autor> listarAutores() {
		return autoresRepository.findAll();
	}
	
	public Autor salvarAutor(Autor autor) {
		if (autor.getId() != null) {
			LOG.info("Salvando autor existente: " + autor.getNome());
			Autor autorDb = autoresRepository.findOne(autor.getId());
			
			if (autorDb != null) {
				throw new AutorExistenteException("O autor que deseja incluir já existe na base.");
			}
		}
		LOG.info("Autor salvo com sucesso: " + autor.getNome());
		return autoresRepository.save(autor);
	}
	
	public Autor buscar(Long id){
		LOG.info("Buscando autor de id: " + id);
		Autor autor = autoresRepository.findOne(id);
		
		if (autor == null) {
			LOG.info("Autor não encontrado.");
			throw new AutorNaoEncontradoException  ("Autor não foi encontrado.");
		}
		LOG.info("Autor encontrado: " + autor.getNome());
		return autor;
	}

	public void update(Autor autor) {
		isThereThisAutor(autor);
		LOG.info("Update no autor: " + autor.getId());
		autoresRepository.save(autor);
	}

	private void isThereThisAutor(Autor autor) {
		buscar(autor.getId());
	}
}
