package br.com.fdantasb.socialbooks.services.exceptions;

public class AutorExistenteException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3702633518512978713L;
	
	public AutorExistenteException(String message, Throwable cause) {
		super(message, cause);
	}

	public AutorExistenteException(String message) {
		super(message);
	}

}
