package br.com.fdantasb.socialbooks.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
public class Autor {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonInclude(Include.NON_NULL)
	private Long id;
	@NotEmpty(message = "O campo nome não pode ser vazio.")
	private String nome;
	@JsonFormat(pattern = "dd/MM/yyyy")
	@NotNull(message = "A data de nascimento é de preenchimento obrigatório.")
	@JsonInclude(Include.NON_NULL)
	private Date nascimento;
	@JsonInclude(Include.NON_NULL)
	@NotNull(message = "A nacionalidade  é de preenchimento obrigatório.")
	private String nacionalidade;
	@JsonIgnore
	@OneToMany(mappedBy = "autor")
	private List<Livro> livros;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getNascimento() {
		return nascimento;
	}
	public void setNascimento(Date nascimento) {
		this.nascimento = nascimento;
	}
	public String getNacionalidade() {
		return nacionalidade;
	}
	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}
	public List<Livro> getLivros() {
		return livros;
	}
	public void setLivros(List<Livro> livros) {
		this.livros = livros;
	}
	
	
}
