package br.com.fdantasb.socialbooks.services.exceptions;

public class LivroNaoEncontradoException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7128718231218105160L;

	public LivroNaoEncontradoException(String message, Throwable cause) {
		super(message, cause);
	}

	public LivroNaoEncontradoException(String message) {
		super(message);
	}
	
}
