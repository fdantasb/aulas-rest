package br.com.fdantasb.socialbooks.resources;

import java.net.URI;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.fdantasb.socialbooks.domain.Comentario;
import br.com.fdantasb.socialbooks.domain.Livro;
import br.com.fdantasb.socialbooks.services.LivrosService;
import sun.rmi.runtime.Log;

@RestController
@RequestMapping(value = "/livros")	
public class LivrosResources {

	private Logger LOG = LoggerFactory.getLogger(LivrosResources.class);
	
	@Autowired
	private LivrosService livrosService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Livro>> listar() {
		LOG.info("Entra no metodo de buscar todos os livros");
		return ResponseEntity.status(HttpStatus.OK).body(livrosService.listar());
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> salvar(@Valid @RequestBody Livro livro) {
		LOG.info("Entra no metodo de salvar livro.");
		livro = livrosService.salvar(livro);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(livro.getId()).toUri();
		LOG.info("URI do livro salvo: " + uri.getPath());
		return ResponseEntity.created(uri).build();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Livro> buscar(@PathVariable Long id) {
		LOG.info("Entra no metodo de busca de livro por id");
		Livro livro = livrosService.buscar(id);
		
		CacheControl cacheControl = CacheControl.maxAge(20, TimeUnit.SECONDS);
		return ResponseEntity.status(HttpStatus.OK).cacheControl(cacheControl).body(livro);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deletar(@PathVariable Long id) {
		LOG.info("Entra no metodo de deletar livro.");
		livrosService.deletar(id);
		LOG.info("Livro deletado com sucesso.");
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(value = "/{ id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizar(@RequestBody Livro livro, @PathVariable("id") Long id) {
		LOG.info("Entra no metodo de update de livro.");
		livro.setId(id);
		livrosService.atualizar(livro);
		LOG.info("Livro atualizado com sucesso.");
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(value = "/{id}/comentarios", method = RequestMethod.POST)
	public ResponseEntity<Void> addComentario(@PathVariable("id") Long id, 
			@RequestBody Comentario comentario) {
		LOG.info("Entra no metodo de adicionar comentario no livro.");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		comentario.setUsuario(auth.getName());
		
		livrosService.salvarComentario(id, comentario);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		LOG.info("URI do comentário adicionado: " + uri.getPath());
		return ResponseEntity.created(uri).build();
	}
	
	@RequestMapping(value = "/{id}/comentarios", method = RequestMethod.GET)
	public ResponseEntity<List<Comentario>> listarcomentarios(@PathVariable("id") Long id){
		LOG.info("Entra no metodo de listar todos os comentarios do livro");
		return ResponseEntity.status(HttpStatus.OK).body(livrosService.comentarios(id));
	}

}
