package br.com.fdantasb.socialbooks.services;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.fdantasb.socialbooks.domain.Comentario;
import br.com.fdantasb.socialbooks.domain.Livro;
import br.com.fdantasb.socialbooks.repository.ComentariosRepository;
import br.com.fdantasb.socialbooks.repository.LivrosRepository;
import br.com.fdantasb.socialbooks.services.exceptions.LivroNaoEncontradoException;

@Service
public class LivrosService {

	private static Logger LOG = LoggerFactory.getLogger(LivrosService.class);
	
	@Autowired
	private LivrosRepository livrosRepository;
	@Autowired
	private ComentariosRepository comentariosRepository;
	
	
	public List<Livro> listar(){
		List<Livro> livroList = livrosRepository.findAll();
		LOG.info("Quantidade de livros encontrada: " + livroList.size());
		return livroList;
	}
	
	public Livro buscar(Long id){
		LOG.info("Busca de livro com o id: " + id);
		Livro livro = livrosRepository.findOne(id);
		
		if (livro == null) {
			LOG.info("Não foi encontrado nenhum livro para o id.");
			throw new LivroNaoEncontradoException("O livro pretendido não pôde ser encontrado.");
		}
		LOG.info("Livro encontrado: " + livro.getNome());
		return livro;
	}
	
	public Livro salvar(Livro livro) {
		livro.setId(null);
		livro = livrosRepository.save(livro);
		LOG.info("Livro salvo com sucesso:" + livro);
		return livro;
	}

	public void deletar(Long id) {
		try{
			LOG.info("Deletando livro id: " + id);
			livrosRepository.delete(id);
		} catch (EmptyResultDataAccessException e) {
			LOG.info("Não foi possível deletar o livro escolhido.");
			throw new LivroNaoEncontradoException("O livro pretendido não pôde ser encontrado.");
		}
		
	}

	public void atualizar(Livro livro) {
		isThereThisBook(livro);
		LOG.info("Atualizando livro.");
		livrosRepository.save(livro);
		LOG.info("Livro atualizado com sucesso.");
	}

	private void isThereThisBook(Livro livro) {
		buscar(livro.getId());
	}
	
	public Comentario salvarComentario(Long idLivro, Comentario comentario) {
		Livro livro = buscar(idLivro);
		LOG.info("Adicionando comentário ao livro.");
		comentario.setLivro(livro);
		comentario.setData(new Date());

		comentario = comentariosRepository.save(comentario);
		LOG.info("Comentário adicionado com sucesso.");
		return comentario;
	}

	public List<Comentario> comentarios(Long idLivro) {
		Livro livro = buscar(idLivro);
		LOG.info("Quantidade de comentários: " + livro.getComentarios().size());
		return livro.getComentarios();
	}

}
