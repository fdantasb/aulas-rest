package br.com.fdantasb.socialbooks.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.fdantasb.socialbooks.domain.Livro;

public interface LivrosRepository extends JpaRepository<Livro, Long>{

}
