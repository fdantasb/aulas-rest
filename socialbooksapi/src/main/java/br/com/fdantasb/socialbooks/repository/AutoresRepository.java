package br.com.fdantasb.socialbooks.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.fdantasb.socialbooks.domain.Autor;

public interface AutoresRepository extends JpaRepository<Autor, Long>{

}
