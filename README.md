# README #

Essa aplicação é o retorno do curso Algaworks para Restful com Spring Aplicado.

### What is this repository for? ###

Esse repositório contém os fontes de todo o curso com branchs separadas para cada aula.

### How do I get set up? ###

Ao contrário do ministrado no curso, está sendo utilizado o Mysql Server como banco de dados, maiores informações e configurações você pode encontrar no arquivo application.properties

### Who do I talk to? ###

Qualquer dúvida, só falar com Flávio Dantas.